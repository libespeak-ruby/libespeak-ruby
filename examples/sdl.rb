require 'espeak'
require 'sdl'

SDL.init SDL::INIT_AUDIO
mixer = SDL::Mixer.open

espeak = Espeak.instance
espeak.set_voice_by_name 'fr'

116.times do |i|
	wave_data = espeak.synth "sloubi #{i + 1}"
	wave = SDL::Mixer::Wave.load_from_io wave_data
	channel = SDL::Mixer.play_channel -1, wave, 0
	sleep 0.5 while SDL::Mixer.play? channel
	wave.destroy
end
