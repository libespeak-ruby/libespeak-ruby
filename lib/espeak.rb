require 'espeak_base'
require 'singleton'
require 'stringio'

class StringIO
	def write_int i
		4.times do
			self.putc i
			i = i >> 8
		end
	end
end

class Espeak < EspeakBase
	include Singleton

	def synth *args, &block
		wave = false
		if block.nil?
			wave = true
			io = StringIO.new '', 'w+'
			io.write "RIFF\x24\xf0\xff\x7fWAVEfmt \x10\x0\x0\x0\x1\x0\x1\x0"
			io.write_int samplerate
			io.write_int samplerate * 2
			io.write "\x2\x0\x10\x00data\x00\xf0\xff\x7f"
			block = lambda do |wav, numsamples, events|
				io.write wav unless wav.nil?
				true
			end
		end
		super *args, &block
		if wave
			size = io.size
			io.seek 4
			io.write_int size - 8
			io.seek 40
			io.write_int size - 44
			io.rewind
			io
		end
	end

end
