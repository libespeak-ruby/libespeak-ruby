#include <stdlib.h>

#include <ruby.h>
#include <espeak/speak_lib.h>

static VALUE event_init(VALUE self, VALUE type, VALUE text_position, VALUE length, VALUE audio_position, VALUE id)
{
	rb_iv_set(self, "@type", type);
	rb_iv_set(self, "@text_position", text_position);
	rb_iv_set(self, "@length", length);
	rb_iv_set(self, "@audio_position", audio_position);
	rb_iv_set(self, "@id", id);
	return self;
}

VALUE Event;

static int synth_callback(short* wav, int numsamples, espeak_EVENT* events)
{
	if (rb_block_given_p())
	{
		VALUE r_events = rb_ary_new();
		for (; events->type != espeakEVENT_LIST_TERMINATED; events++)
		{
			if (events->type == espeakEVENT_SAMPLERATE)
				break;
			VALUE argv[5];
			switch(events->type)
			{
				case espeakEVENT_WORD:
					argv[0] = ID2SYM(rb_intern("word"));
					break;
				case espeakEVENT_SENTENCE:
					argv[0] = ID2SYM(rb_intern("sentence"));
					break;
				case espeakEVENT_MARK:
					argv[0] = ID2SYM(rb_intern("mark"));
					break;
				case espeakEVENT_PLAY:
					argv[0] = ID2SYM(rb_intern("play"));
					break;
				case espeakEVENT_END:
					argv[0] = ID2SYM(rb_intern("end"));
					break;
				case espeakEVENT_MSG_TERMINATED:
					argv[0] = ID2SYM(rb_intern("msg_terminated"));
					break;
				case espeakEVENT_PHONEME:
					argv[0] = ID2SYM(rb_intern("phoneme"));
					break;
			}
			argv[1] = INT2NUM(events->text_position);
			argv[2] = INT2NUM(events->length);
			argv[3] = INT2NUM(events->audio_position);
			if (events->type == espeakEVENT_WORD || events->type == espeakEVENT_SENTENCE || events->type == espeakEVENT_PHONEME)
				argv[4] = INT2NUM(events->id.number);
			else if (events->type == espeakEVENT_MARK || events->type == espeakEVENT_PLAY)
				argv[4] = rb_str_new2(events->id.name);
			else
				argv[4] = Qnil;
			VALUE r_event = rb_class_new_instance(5, argv, Event);
			rb_ary_push(r_events, r_event);
		}
		VALUE r_wav;
		if (wav == NULL)
			r_wav = Qnil;
		else
			r_wav = rb_str_new((const char*) wav, numsamples * 2);
		VALUE args = rb_ary_new3(3, r_wav, INT2NUM(numsamples), r_events);
		VALUE should_continue = rb_yield(args);
		return (should_continue == Qnil || should_continue == Qfalse ? 1 : 0);
	}
	return 0;
}

static VALUE espeak_init(VALUE self)
{
	int samplerate = espeak_Initialize(AUDIO_OUTPUT_SYNCHRONOUS, 0, NULL, 0);
	rb_iv_set(self, "@samplerate", INT2NUM(samplerate));
	espeak_SetSynthCallback(synth_callback);
	return self;
}

static VALUE espeak_synth(int argc, VALUE* argv, VALUE self)
{
	VALUE text, position, position_type, end_position;
	int nb_args = rb_scan_args(argc, argv, "13", &text, &position, &position_type, &end_position);
	if (nb_args < 2)
		position = INT2NUM(0);
	if (nb_args < 3)
		position_type = ID2SYM(rb_intern("character"));
	if (nb_args < 4)
		end_position = INT2NUM(0);

	VALUE text_str = StringValue(text);

	const char* position_type_str = rb_id2name(SYM2ID(position_type));
	espeak_POSITION_TYPE espeak_position = 0;
	if (strcmp(position_type_str, "character") == 0)
		espeak_position = POS_CHARACTER;
	else if (strcmp(position_type_str, "word") == 0)
		espeak_position = POS_WORD;
	else if (strcmp(position_type_str, "sentence") == 0)
		espeak_position = POS_SENTENCE;

	espeak_Synth(RSTRING_PTR(text_str), RSTRING_LEN(text_str) + 1, NUM2UINT(position), espeak_position, NUM2UINT(end_position), espeakCHARS_AUTO, NULL, NULL);
	return Qnil;
}

static VALUE espeak_set_voice_by_name(VALUE self, VALUE name)
{
	espeak_SetVoiceByName(StringValuePtr(name));
	return Qnil;
}

VALUE EspeakBase;

void Init_espeak_base()
{
	EspeakBase = rb_define_class("EspeakBase", rb_cObject);
	rb_require("singleton");

	rb_define_attr(EspeakBase, "samplerate", 1, 0);

	rb_define_method(EspeakBase, "initialize", espeak_init, 0);
	rb_define_method(EspeakBase, "synth", espeak_synth, -1);
	rb_define_method(EspeakBase, "set_voice_by_name", espeak_set_voice_by_name, 1);

	Event = rb_define_class_under(EspeakBase, "Event", rb_cObject);

	rb_define_attr(Event, "type", 1, 0);
	rb_define_attr(Event, "unique_identifier", 1, 0);
	rb_define_attr(Event, "text_position", 1, 0);
	rb_define_attr(Event, "length", 1, 0);
	rb_define_attr(Event, "audio_position", 1, 0);
	rb_define_attr(Event, "sample", 1, 0);
	rb_define_attr(Event, "id", 1, 0);

	rb_define_method(Event, "initialize", event_init, 5);
}
